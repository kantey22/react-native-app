import React from 'react';
import PlaceMap from './place_map.js';
import { AppRegistry, Text, View,TabBarIOS, StyleSheet,MapView,Alert,TextInput, Button,AsyncStorage} from 'react-native';

export default class App extends React.Component {

   constructor(props) {
  super(props);
  this.state = {
    selectedTab: 0,
    text: '',
    latitude: '',
    longitude: '',
    error: '',

  };
}

  componentDidMount() {
    AsyncStorage.getItem("myKey").then((value) =>{
      this.setState({"myKey": value});
    }).done();
  

    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
    );


  }
  
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }

  onPress() {
  Alert.alert('save note '+this.state.text+'\n my longitude '+this.state.longitude+ '& \n my latitude' + this.state.latitude);
 }
 getInitialState()
 {
   return{};
 }


handleTabPress(tab) {
  this.setState({ selectedTab: tab })
}

  render() {
    return (
     <TabBarIOS>
        <TabBarIOS.Item
          icon={require('./assets/notes.png')}
          title="View Notes"
          selected={this.state.selectedTab === 0}
          onPress={this.handleTabPress.bind(this, 0)}
        >
      
     

               <View style={styles.view}>
            <Text style={styles.heading}>List of Notes Added</Text>
           
              
            
          </View>
        
        </TabBarIOS.Item>

        <TabBarIOS.Item
          title="Add Notes"
          icon={require('./assets/addNote.png')}
          selected={this.state.selectedTab === 1}
          onPress={this.handleTabPress.bind(this, 1)}
        >
          <View style={styles.view}>
            <Text style={styles.heading}>Add Notes</Text>
            <TextInput style={styles.textInput}
               onChangeText={(text) => this.setState({text})}
               value={this.state.text}
             />

             <Text style={styles.text}>My latitude : {this.state.latitude} </Text>
             <Text style={styles.text}>My longitude : {this.state.longitude} </Text>

               <View style={styles.buttonContainer}>
                   <Button onPress={this.onPress.bind(this)} title="Add Note" color="#FFFFFF" accessibilityLabel="Tap on Me"/>
                 </View>

            
          </View>
          
        </TabBarIOS.Item>
</TabBarIOS>

      
    );
  }



}

const styles = StyleSheet.create({

 view: {
    backgroundColor: 'red',
    flex: 1,
   
   },
heading:{
  fontSize:20,
  marginTop:50,
  marginBottom: 50,
  textAlign:'center',
  color: '#fff',
  fontWeight: 'bold'
},
  text: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    margin:5
    
  },

  textInput:{
     height: 100,
     borderColor: '#000',
      borderWidth: 1,
     textAlign:'center',
     marginRight:20,
     marginLeft: 20,
     color: '#000',
     backgroundColor: '#fff'

    

   },
    buttonContainer: {
    backgroundColor: '#2E9298',
    borderRadius: 10,
    padding: 10,
    shadowColor: '#00000',
    margin:20,
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 10,
    shadowOpacity: 0.25
  }
});